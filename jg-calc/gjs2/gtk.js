const Lang = imports.lang;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

Gtk.init(null, 0);
let builder = new Gtk.Builder();
builder.add_from_file( "ui.glade" );

builder.connect_signals(this);

// let signal_handlers = new SignalHandlers();
// ui.connect_signals_full(Lang.bind(signal_handlers, signal_handlers._connector));

Gtk.main();

function on_button_ok_clicked() {
    print("ok");
};


// const SignalHandlers = new Lang.Class({
//     Name: 'SignalHandlers',

//     _init: function () {},

//     your_switch_active_notify_cb: function (object) {
//       print("switch is active:" + object.get_active());
//     },

//     _connector: function(builder, object, signal, handler) {
//       object.connect(signal, Lang.bind(this, this[handler]));
//     },
// });

// function buildPrefsWidget() {
//   let win = new Gtk.Table(), ui = new Gtk.Builder();

//   ui.add_from_file("ui.glade");
//   ui.get_object("content-table").reparent(win);

//   let signal_handlers = new SignalHandlers();
//   ui.connect_signals_full(Lang.bind(signal_handlers, signal_handlers._connector));

//   return win;
// }