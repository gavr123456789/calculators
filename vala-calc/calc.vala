using Gtk;
using matheval;

struct CalculatorStates {
	public CalculatorStates(){
		first_phase  = true;
		second_phase = false;
		(a,b,oprtr,rvalue) = new string[] {"","","",""};
	}

	public void reset(){
		a      = "\0";
		b      = "\0";
		oprtr  = "\0";
		rvalue = "\0";
	}

	public string rvalue;
	public string a;
	public string b;
	public string oprtr;

	public bool first_phase;
	public bool second_phase;
}

[GtkTemplate (ui = "/org/gavr/calc/calc.ui")]
public class MainWidget : ApplicationWindow {
	CalculatorStates calc;

	[GtkChild]
	Entry entry;

	[GtkChild]
	public MainWidget () {
		calc = CalculatorStates();
		message(@"$(calc.first_phase)");
		this.show_all();
		this.destroy.connect (Gtk.main_quit);
	}

	[GtkCallback]
	void number_clicked (Button btn){

		//если только что нажимали равно то нужно всё стереть
		if(!calc.first_phase && !calc.second_phase) 
		{
			calc.reset();
			calc.first_phase = true;
		}
		if (calc.first_phase) entry.set_text(calc.a += btn.get_label());
		if (!calc.first_phase && calc.second_phase)
			entry.set_text(calc.a + " " + calc.oprtr + " " + (calc.b += btn.get_label()));
		
	}

	[GtkCallback]
	void operator_clicked (Button btn){
		calc.first_phase = false;
		calc.second_phase = true;
		calc.oprtr = btn.get_label();
		entry.set_text(calc.a + " " + calc.oprtr);
	}

	[GtkCallback]
	void equel_clicked (Button btn){
		calc.first_phase = false;
		calc.second_phase = false;
		int a = int.parse(calc.a);
		int b = int.parse(calc.b);
		
		if (calc.oprtr == "+") calc.rvalue = @"$(a + b)";
		if (calc.oprtr == "-") calc.rvalue = @"$(a - b)";
		if (calc.oprtr == "*") calc.rvalue = @"$(a * b)";
		if (calc.oprtr == "/") calc.rvalue = @"$(a / b)";
		
		entry.set_text(calc.a + " " + calc.oprtr + " " + calc.b + " = " + calc.rvalue);
	}
}

void main(string[] args) {
	Gtk.init (ref args);
	var widget = new MainWidget ();
	Gtk.main ();
}
